# Curso Java

## Temario

### Java class design
 
- Inheritance
- Polymorphism 
- Override methods
- Overload constructors and methods
- Use the instanceof operator and casting
- Use virtual method invocation
- Override the hashCode, equals, and toString methods from the Object class to improve the functionality of your class
 
### Generics and collections
 
- Identify when and how to apply abstract classes
- Construct abstract Java classes and subclasses
- Create top-level and nested classes
- Create a generic class
- Use the diamond for type inference
- Analyze the interoperability of collections that use raw types and generic types
- Use wrapper classes, autoboxing and unboxing
- Create and use List, Set and Deque implementations
- Create and use Map implementations
- Use java.util.Comparator and java.lang.Comparable
- Sort and search arrays and lists
 
### Exceptions and assertions 
 
- Use throw and throws statements
- Develop code that handles multiple Exception types in a single catch block
- Develop code that uses try-with-resources statements (including using classes that implement the AutoCloseable interface)
- Create custom exceptions

### JUnit

- JUnit
- Coverage
- Mock

### Maven

- Life cycle
⋅ Compile
⋅ Testing
⋅ Packaging
⋅ Install
⋅ Deploy
- Dependencies
- Plugins


### CI

- Jenkins
- Sonar
⋅ Vulnerabilities
⋅ Bugs
⋅ Code smells
⋅ Technical debt
 

### Acceso a datos

- JDBC
- JPA
- QueryDSL

### Web

- Servlet
- Servicios Rest 
⋅ Jersey
⋅ SpringRest


