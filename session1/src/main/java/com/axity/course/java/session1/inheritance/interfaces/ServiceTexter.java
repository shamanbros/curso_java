package com.axity.course.java.session1.inheritance.interfaces;

public interface ServiceTexter extends Service, Texter, Changer
{
  public static final long MIO = 1;
  public abstract void sayHello( String person );
}
