package com.axity.course.java.session1.service;

import java.util.List;

public interface SalesService
{

  List<Sale> findSales( Filter filter, SaleOrder saleOrder );
}
