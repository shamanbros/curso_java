package com.axity.course.java.session1.inheritance;

/**
 * Enumeración de tipos de persona
 * 
 * @author gsegura
 */
public enum PersonType
{
  ADMINISTRATOR(1, "A"), STUDENT(2, "S"), PROFESSOR(3, "P"), TUTOR(4, "T");
  private int id;
  private String code;

  /**
   * Constructor privado por id y código
   * 
   * @param id
   * @param code
   */
  private PersonType( int id, String code )
  {
    this.id = id;
    this.code = code;
  }

  /**
   * @return the id
   */
  public int getId()
  {
    return id;
  }

  /**
   * @return the code
   */
  public String getCode()
  {
    return code;
  }

  /**
   * Obtiene un tipo de persona por su id
   * 
   * @param id
   * @return
   */
  public static PersonType fromId( int id )
  {
    PersonType personType = null;
    for( PersonType pt : values() )
    {
      if( pt.id == id )
      {
        personType = pt;
        break;
      }
    }
    return personType;
  }

  /**
   * Obtiene un tipo de persona por su código
   * 
   * @param code
   * @return
   */
  public static PersonType fromCode( String code )
  {
    PersonType personType = null;
    for( PersonType pt : values() )
    {
      if( pt.code.equals( code ) )
      {
        personType = pt;
        break;
      }
    }
    return personType;
  }

}
