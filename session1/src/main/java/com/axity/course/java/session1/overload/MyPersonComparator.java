package com.axity.course.java.session1.overload;

import java.util.Comparator;

public class MyPersonComparator implements Comparator<MyPerson>
{

  @Override
  public int compare( MyPerson o1, MyPerson o2 )
  {
    int compare = 1;
    if( o2 != null )
    {
      compare = Boolean.valueOf( o1.isActive() ).compareTo( Boolean.valueOf( o2.isActive() ) );
    }
    return 0;
  }

}
