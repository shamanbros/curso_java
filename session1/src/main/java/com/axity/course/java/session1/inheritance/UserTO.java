package com.axity.course.java.session1.inheritance;

import java.util.Date;
import java.util.List;

/**
 * Clase de trasnferencia de usuarios
 * 
 * @author gsegura
 */
public class UserTO extends IdTO
{

  private static final long serialVersionUID = 2869092079011226032L;
  private String username;
  private String password;
  private List<RoleTO> roles;
  private PersonTO person;
  private Date lastLogin;

  /**
   * @return the username
   */
  public String getUsername()
  {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername( String username )
  {
    this.username = username;
  }

  /**
   * @return the password
   */
  public String getPassword()
  {
    return password;
  }

  /**
   * @param password the password to set
   */
  public void setPassword( String password )
  {
    this.password = password;
  }

  /**
   * @return the roles
   */
  public List<RoleTO> getRoles()
  {
    return roles;
  }

  /**
   * @param roles the roles to set
   */
  public void setRoles( List<RoleTO> roles )
  {
    this.roles = roles;
  }

  /**
   * @return the person
   */
  public PersonTO getPerson()
  {
    return person;
  }

  /**
   * @param person the person to set
   */
  public void setPerson( PersonTO person )
  {
    this.person = person;
  }

  /**
   * @return the lastLogin
   */
  public Date getLastLogin()
  {
    return lastLogin;
  }

  /**
   * @param lastLogin the lastLogin to set
   */
  public void setLastLogin( Date lastLogin )
  {
    this.lastLogin = lastLogin;
  }

}
