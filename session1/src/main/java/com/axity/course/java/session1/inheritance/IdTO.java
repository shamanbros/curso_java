package com.axity.course.java.session1.inheritance;

import java.io.Serializable;

/**
 * Clase de transferencia con id
 * 
 * @author gsegura
 */
public class IdTO extends BaseTO
{

  private static final long serialVersionUID = -8256505415819522242L;
  protected Long id;

  /**
   * Constructor default
   */
  public IdTO()
  {
    super();
  }

  /**
   * COnstructor por id
   * 
   * @param id
   */
  public IdTO( Long id )
  {
    super();
    this.id = id;
  }

  /**
   * @return the id
   */
  public Long getId()
  {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId( Long id )
  {
    this.id = id;
  }

}
