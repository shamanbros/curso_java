package com.axity.course.java.session1.service;

public class Catalog
{
  protected String code;
  protected String data;

  public Catalog()
  {
  }

  public Catalog( String code, String data )
  {
    this.code = code;
    this.data = data;
  }

  /**
   * @return the code
   */
  public String getCode()
  {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode( String code )
  {
    this.code = code;
  }

  /**
   * @return the data
   */
  public String getData()
  {
    return data;
  }

  /**
   * @param data the data to set
   */
  public void setData( String data )
  {
    this.data = data;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "{ \"code\" : \"" ).append( this.code );
    sb.append( "\", \"data\" : \"" ).append( this.data ).append( "\" }" );
    return sb.toString();
  }
}
