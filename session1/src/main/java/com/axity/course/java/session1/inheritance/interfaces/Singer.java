package com.axity.course.java.session1.inheritance.interfaces;

public interface Singer
{

  void sing();
}
