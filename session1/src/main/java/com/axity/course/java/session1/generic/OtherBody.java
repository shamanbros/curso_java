package com.axity.course.java.session1.generic;

public class OtherBody extends Body
{

  private int b;

  /**
   * @return the b
   */
  public int getB()
  {
    return b;
  }

  /**
   * @param b the b to set
   */
  public void setB( int b )
  {
    this.b = b;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "[ a : " ).append( a );
    sb.append( ", b : " ).append( b ).append( "]" );
    return sb.toString();
  }

}
