package com.axity.course.java.session1.generic;

public class Header
{
  private int id;
  private String code;

  /**
   * @return the id
   */
  public int getId()
  {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId( int id )
  {
    this.id = id;
  }

  /**
   * @return the code
   */
  public String getCode()
  {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode( String code )
  {
    this.code = code;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "[ id :" ).append( this.id ).append( ", " );
    sb.append( "code :" ).append( this.code ).append( "]" );
    return sb.toString();
  }
}
