package com.axity.course.java.session1.inheritance;

import java.util.List;

/**
 * Clase de transferencia para el horario
 * 
 * @author gsegura
 */
public class ScheduleTO extends BaseTO
{

  /**
   * 
   */
  private static final long serialVersionUID = -1027422774722592392L;
  private List<ScheduleDateTO> scheduleDates;

  /**
   * @return the scheduleDates
   */
  public List<ScheduleDateTO> getScheduleDates()
  {
    return scheduleDates;
  }

  /**
   * @param scheduleDates the scheduleDates to set
   */
  public void setScheduleDates( List<ScheduleDateTO> scheduleDates )
  {
    this.scheduleDates = scheduleDates;
  }

}
