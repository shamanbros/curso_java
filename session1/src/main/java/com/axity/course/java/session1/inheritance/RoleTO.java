package com.axity.course.java.session1.inheritance;

import java.util.List;

public class RoleTO extends CatalogTO
{

  private String description;
  private List<MenuTO> menus;

  /**
   * @return the description
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription( String description )
  {
    this.description = description;
  }

  /**
   * @return the menus
   */
  public List<MenuTO> getMenus()
  {
    return menus;
  }

  /**
   * @param menus the menus to set
   */
  public void setMenus( List<MenuTO> menus )
  {
    this.menus = menus;
  }

}
