package com.axity.course.java.session1.inheritance;

import java.io.Serializable;

/**
 * Clase base de objetos de transferencia
 * 
 * @author gsegura
 */
public class BaseTO implements Serializable
{

  private static final long serialVersionUID = 4695454404514382770L;
  protected Long userId;
  protected String lang;
  protected boolean active = true;
  protected String ipAddress;
  /**
   * @return the userId
   */
  public Long getUserId()
  {
    return userId;
  }
  /**
   * @param userId the userId to set
   */
  public void setUserId( Long userId )
  {
    this.userId = userId;
  }
  /**
   * @return the lang
   */
  public String getLang()
  {
    return lang;
  }
  /**
   * @param lang the lang to set
   */
  public void setLang( String lang )
  {
    this.lang = lang;
  }
  /**
   * @return the active
   */
  public boolean isActive()
  {
    return active;
  }
  /**
   * @param active the active to set
   */
  public void setActive( boolean active )
  {
    this.active = active;
  }
  /**
   * @return the ipAddress
   */
  public String getIpAddress()
  {
    return ipAddress;
  }
  /**
   * @param ipAddress the ipAddress to set
   */
  public void setIpAddress( String ipAddress )
  {
    this.ipAddress = ipAddress;
  }
  /**
   * @return the serialversionuid
   */
  public static long getSerialversionuid()
  {
    return serialVersionUID;
  }
  
  

}
