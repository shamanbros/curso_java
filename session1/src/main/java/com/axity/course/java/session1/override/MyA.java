package com.axity.course.java.session1.override;

public class MyA extends A
{

  @Override
  public String toString()
  {
    String a = super.toString();
    return "My " + a;
  }
}
