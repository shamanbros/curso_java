package com.axity.course.java.session1.inheritance.interfaces.impl;

import com.axity.course.java.session1.inheritance.interfaces.Texter;

public class TexterImpl implements Texter
{

  @Override
  public void text( String text )
  {
    System.out.println( "Texting " + text );

  }

}
