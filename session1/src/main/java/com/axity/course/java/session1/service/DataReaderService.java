package com.axity.course.java.session1.service;

import java.util.List;

public interface DataReaderService
{

  List<String> readData(String filename);
}
