package com.axity.course.java.session1.inheritance;

import java.util.Date;

/**
 * Clase de transferencia de personas
 * 
 * @author gsegura
 */
public class PersonTO extends IdTO
{

  private static final long serialVersionUID = 7894871188445364723L;
  protected String name;
  protected String lastname;
  protected Date birthday;
  protected PersonType personType;
  protected String phone;

  /**
   * Constructor default
   */
  public PersonTO()
  {
    super();
  }

  /**
   * Constructor por id
   * 
   * @param id
   */
  public PersonTO( Long id )
  {
    super( id );
  }

  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName( String name )
  {
    this.name = name;
  }

  /**
   * @return the lastname
   */
  public String getLastname()
  {
    return lastname;
  }

  /**
   * @param lastname the lastname to set
   */
  public void setLastname( String lastname )
  {
    this.lastname = lastname;
  }

  /**
   * @return the birthday
   */
  public Date getBirthday()
  {
    return birthday;
  }

  /**
   * @param birthday the birthday to set
   */
  public void setBirthday( Date birthday )
  {
    this.birthday = birthday;
  }

  /**
   * @return the personType
   */
  public PersonType getPersonType()
  {
    return personType;
  }

  /**
   * @param personType the personType to set
   */
  public void setPersonType( PersonType personType )
  {
    this.personType = personType;
  }

  /**
   * @return the phone
   */
  public String getPhone()
  {
    return phone;
  }

  /**
   * @param phone the phone to set
   */
  public void setPhone( String phone )
  {
    this.phone = phone;
  }

}
