package com.axity.course.java.session1.inheritance.interfaces;

public interface Changer
{

  /**
   * Sets the singer
   * 
   * @param doable
   */
  void setSinger( Singer singer );

  /**
   * Sets the texter
   * 
   * @param texter
   */
  void setTexter( Texter texter );

  /**
   * Sets the service
   * 
   * @return
   */
  void setService( Service service );
}
