package com.axity.course.java.session1.inheritance;

import java.io.Serializable;

/**
 * Clase de transferncia de horario por dia
 * 
 * @author gsegura
 */
public class ScheduleDateTO implements Serializable
{

  private static final long serialVersionUID = 8664588161414208345L;
  private int day;
  private int hour;
  private int minute;
  private int duration;

  // TODO crear constructores, getter/setters, hashCode, equals, toString

}
