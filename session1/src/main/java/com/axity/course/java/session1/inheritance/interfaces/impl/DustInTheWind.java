package com.axity.course.java.session1.inheritance.interfaces.impl;

import com.axity.course.java.session1.inheritance.interfaces.Singer;

public class DustInTheWind implements Singer
{

  @Override
  public void sing()
  {
    System.out.println( "I close my eyes, only for a moment, and the moment's gone" );
    System.out.println( "All my dreams pass before my eyes, a curiosity" );
    System.out.println( "Dust in the wind" );
    System.out.println( "All they are is dust in the wind" );

  }

}
