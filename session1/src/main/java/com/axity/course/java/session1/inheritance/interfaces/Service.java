package com.axity.course.java.session1.inheritance.interfaces;

public interface Service extends Singer
{

  void serve( int id );
}
