package com.axity.course.java.session1.inheritance;

import java.util.Comparator;

public class CatalogComparator implements Comparator<CatalogTO>
{

  @Override
  public int compare( CatalogTO o1, CatalogTO o2 )
  {
    return o1.name.compareTo( o2.name );
  }

}
