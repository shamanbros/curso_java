package com.axity.course.java.session1.service;

public class Filter
{

  private Type type;
  private String data;

  public enum Type
  {
    COUNTRY, PRODUCT, DATE, SALES_GTE, SALES_LTE
  }

  /**
   * @return the type
   */
  public Type getType()
  {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType( Type type )
  {
    this.type = type;
  }

  /**
   * @return the data
   */
  public String getData()
  {
    return data;
  }

  /**
   * @param data the data to set
   */
  public void setData( String data )
  {
    this.data = data;
  }

}
