package com.axity.course.java.session1.inheritance;

import java.util.List;

/**
 * Clase de transferencia de cursos
 * 
 * @author gsegura
 */
public class CourseTO extends IdTO
{

  private SubjectTO subject;
  private List<ScheduleTO> schedule;
  private RoomTO room;
  private TeacherTO teacher;

  // TODO crear constructores, getter/setters, hashCode, equals, toString

}
