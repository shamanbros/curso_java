package com.axity.course.java.session1.casting;

public class AB extends A
{
  public int b;

  @Override
  public String toString()
  {
    return new StringBuilder( "{ \"a\" : " ).append( this.a ).append( ", \"b\" : " ).append( this.b ).append( " }" )
        .toString();
  }
}
