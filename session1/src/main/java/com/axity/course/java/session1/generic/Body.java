package com.axity.course.java.session1.generic;

public class Body
{
  protected int a;

  /**
   * @return the a
   */
  public int getA()
  {
    return a;
  }

  /**
   * @param a the a to set
   */
  public void setA( int a )
  {
    this.a = a;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "[ a : " ).append( a ).append( "]" );
    return sb.toString();
  }

}
