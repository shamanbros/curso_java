package com.axity.course.java.session1.service;

public enum SaleOrder
{

  COUNTRY_ASC, COUNTRY_DESC, PRODUCT_ASC, PRODUCT_DESC, DATE_ASC, DATE_DESC, SALES_ASC, SALES_DESC;

}
