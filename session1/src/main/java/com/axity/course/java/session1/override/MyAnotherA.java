package com.axity.course.java.session1.override;

public class MyAnotherA extends MyA
{
  @Override
  public String toString()
  {
    return "MyAnother A";
  }
}
