package com.axity.course.java.session1.inheritance.interfaces.impl;

import com.axity.course.java.session1.inheritance.interfaces.Singer;
import com.axity.course.java.session1.inheritance.interfaces.Service;
import com.axity.course.java.session1.inheritance.interfaces.ServiceTexter;
import com.axity.course.java.session1.inheritance.interfaces.Texter;

public class ServiceTexterImpl implements ServiceTexter
{
  private Singer singer;
  private Texter texter;
  private Service service;

  public ServiceTexterImpl()
  {
    singer = new DuHast();
    texter = new TexterImpl();
    service = new ServiceImpl();
  }

  @Override
  public void serve( int id )
  {
    service.serve( id );
  }

  @Override
  public void sing()
  {
    singer.sing();

  }

  @Override
  public void text( String text )
  {
    texter.text( text );
  }

  @Override
  public void sayHello( String person )
  {
    System.out.println( "Hello " + person );
  }

  /**
   * @param doable the doable to set
   */
  public void setSinger( Singer doable )
  {
    this.singer = doable;
  }

  /**
   * @param texter the texter to set
   */
  public void setTexter( Texter texter )
  {
    this.texter = texter;
  }

  /**
   * @return the service
   */
  public void setService( Service service )
  {
    this.service = service;
  }

}
