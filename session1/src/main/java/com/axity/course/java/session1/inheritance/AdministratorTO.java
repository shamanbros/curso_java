package com.axity.course.java.session1.inheritance;

/**
 * Clase de transferencia de las personas administrativas
 * 
 * @author gsegura
 */
public class AdministratorTO extends PersonTO
{

  /**
   * 
   */
  private static final long serialVersionUID = 4189662144360586924L;
  private AreaTO area;
  private String phone;

  /**
   * Constructor default
   */
  public AdministratorTO()
  {
    super();
    this.personType = PersonType.ADMINISTRATOR;
  }

  /**
   * Constructor por id
   * 
   * @param id
   */
  public AdministratorTO( Long id )
  {
    super( id );
    this.personType = PersonType.ADMINISTRATOR;
  }

  /**
   * @return the area
   */
  public AreaTO getArea()
  {
    return area;
  }

  /**
   * @param area the area to set
   */
  public void setArea( AreaTO area )
  {
    this.area = area;
  }

  /**
   * @return the phone
   */
  public String getPhone()
  {
    return phone;
  }

  /**
   * @param phone the phone to set
   */
  public void setPhone( String phone )
  {
    this.phone = phone;
  }

}
