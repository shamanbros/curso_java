package com.axity.course.java.session1.service.impl;

import java.util.Comparator;

import com.axity.course.java.session1.service.Sale;

public class SaleSalesComparator implements Comparator<Sale>
{

  private boolean asc;

  public SaleSalesComparator()
  {
    this.asc = true;
  }

  public SaleSalesComparator( boolean asc )
  {
    this.asc = asc;
  }

  @Override
  public int compare( Sale o1, Sale o2 )
  {
    int compare = 1;
    if( o1 != null && o2 != null )
    {
      if( asc )
      {
        if( o1.getSales() > o2.getSales() )
        {
          compare = 1;
        }
        else if( o1.getSales() < o2.getSales() )
        {
          compare = -1;
        }
        else
        {
          compare = 0;
        }

      }
      else
      {
        if( o2.getSales() > o1.getSales() )
        {
          compare = 1;
        }
        else if( o2.getSales() < o1.getSales() )
        {
          compare = -1;
        }
        else
        {
          compare = 0;
        }
      }

    }

    return compare;
  }

}
