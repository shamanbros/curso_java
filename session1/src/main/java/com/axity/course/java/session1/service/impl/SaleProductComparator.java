package com.axity.course.java.session1.service.impl;

import java.util.Comparator;

import com.axity.course.java.session1.service.Sale;

public class SaleProductComparator implements Comparator<Sale>
{

  private boolean asc;

  public SaleProductComparator()
  {
    this.asc = true;
  }

  public SaleProductComparator( boolean asc )
  {
    this.asc = asc;
  }

  @Override
  public int compare( Sale o1, Sale o2 )
  {
    int compare = 1;
    if( o1 != null && o1.getProduct() != null && o2 != null && o2.getProduct() != null )
    {
      if( asc )
      {
        compare = o1.getProduct().getCode().compareTo( o2.getProduct().getCode() );
      }
      else
      {
        compare = o2.getProduct().getCode().compareTo( o1.getProduct().getCode() );
      }

    }

    return compare;
  }

}
