package com.axity.course.java.session1.casting;

public class A extends AbstractLetter
{
  public int a;

  @Override
  public String toString()
  {
    return new StringBuilder( "{ \"a\" : " ).append( this.a ).append( " }" ).toString();
  }
}
