package com.axity.course.java.session1.casting;

public class AC extends A
{
  public int c;

  @Override
  public String toString()
  {
    return new StringBuilder( "{ \"a\" : " ).append( this.a ).append( ", \"c\" : " ).append( this.c ).append( " }" )
        .toString();
  }
}
