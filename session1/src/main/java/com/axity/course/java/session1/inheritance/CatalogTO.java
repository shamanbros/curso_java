package com.axity.course.java.session1.inheritance;

/**
 * Clase de transferencia para catálogos genéricos
 * 
 * @author gsegura
 */
public class CatalogTO extends IdTO
{

  private static final long serialVersionUID = -6056227893675554794L;
  protected String name;

  /**
   * Constructor default
   */
  public CatalogTO()
  {
    super();
  }

  /**
   * Constructor por id
   * 
   * @param id
   */
  public CatalogTO( Long id )
  {
    super( id );
  }

  /**
   * Constructor por nombre
   * 
   * @param name
   */
  public CatalogTO( String name )
  {
    super();
    this.name = name;
    init();
  }

  private void init()
  {
    // algo

  }

  /**
   * Constructor por id y nombre
   * 
   * @param id
   * @param name
   */
  public CatalogTO( Long id, String name )
  {
    super( id );
    this.name = name;
  }

  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName( String name )
  {
    this.name = name;
  }

  @Override
  public String toString()
  {
    return new StringBuilder().append( "id:" ).append( id ).append( ", name: " ).append( name ).toString();
  }
}
