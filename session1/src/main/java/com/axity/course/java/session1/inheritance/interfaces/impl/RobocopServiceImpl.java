package com.axity.course.java.session1.inheritance.interfaces.impl;

import com.axity.course.java.session1.inheritance.interfaces.Service;
import com.axity.course.java.session1.inheritance.interfaces.Singer;

public class RobocopServiceImpl implements Service
{
  private Singer singer;

  public RobocopServiceImpl()
  {
    init();
  }

  private void init()
  {
    System.out.println( "Initializing Robocop Service" );

  }

  public RobocopServiceImpl( Singer singer )
  {
    init();
    if( singer != null )
    {
      System.out.println( "Robocop knows how to sing now!" );
      this.singer = singer;
    }
  }

  @Override
  public void sing()
  {
    if( singer == null )
    {
      throw new RuntimeException( "Robocop doesn't sing citizen!!!" );
    }
    System.out.println( "Robocop sings:" );
    singer.sing();
  }

  @Override
  public void serve( int id )
  {
    System.out.println( "Directive " + id + ":" );
    switch( id )
    {
      case 1:
        System.out.println( "  Serve the public trust" );
        break;
      case 2:
        System.out.println( "  Protect the innocent" );
        break;
      case 3:
        System.out.println( "  Uphold the law" );
        break;
      case 4:
        System.out.println( "  Any attempt to arrest a senior officer of OCP results in shutdown" );
        break;
      default:
        System.out.println( "  Not available directive " );
    }

  }

}
