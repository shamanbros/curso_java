package com.axity.course.java.session1.inheritance;

public class MenuTO extends CatalogTO
{

  /**
   * 
   */
  private static final long serialVersionUID = 6094797697549617971L;
  private String icon;
  private String url;
  private String valorExtra;

  /**
   * Constructor default
   */
  public MenuTO()
  {
    super();
  }

  /**
   * Constructor por id
   * 
   * @param id
   */
  public MenuTO( Long id )
  {
    super( id );
  }

  /**
   * Constructor por nombre
   * 
   * @param name
   */
  public MenuTO( String name )
  {
    super( name );
  }

  /**
   * Constructor por id y nombre
   * 
   * @param id
   * @param name
   */
  public MenuTO( Long id, String name )
  {
    super( id, name );
  }

  /**
   * Constructor por id, nombre, ícono y url
   * 
   * @param id
   * @param name
   * @param icon
   * @param url
   */
  public MenuTO( Long id, String name, String icon, String url )
  {
    super( id, name );
    this.icon = icon;
    this.url = url;
  }

  /**
   * @return the icon
   */
  public String getIcon()
  {
    return icon;
  }

  /**
   * @param icon the icon to set
   */
  public void setIcon( String icon )
  {
    this.icon = icon;
  }

  /**
   * @return the url
   */
  public String getUrl()
  {
    return url;
  }

  /**
   * @param url the url to set
   */
  public void setUrl( String url )
  {
    this.url = url;
  }

  /**
   * @return the valorExtra
   */
  public String getValorExtra()
  {
    return valorExtra;
  }

  /**
   * @param valorExtra the valorExtra to set
   */
  public void setValorExtra( String valorExtra )
  {
    this.valorExtra = valorExtra;
  }

}
