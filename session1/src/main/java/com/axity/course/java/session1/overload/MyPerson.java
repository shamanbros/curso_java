package com.axity.course.java.session1.overload;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase ejemplo de sobrecarga de constructores
 * 
 * @author gsegura
 */
public class MyPerson
{

  private String name;
  private String lastname;
  private Date birthday;
  private String phone;
  private boolean active;

  /**
   * @return the active
   */
  public boolean isActive()
  {
    return active;
  }

  /**
   * @param active the active to set
   */
  public void setActive( boolean active )
  {
    this.active = active;
  }

  /**
   * Constructor default
   */
  public MyPerson()
  {
  }

  /**
   * Constructor por nombre
   * 
   * @param name
   */
  public MyPerson( String name )
  {
    this.name = name;
  }

  /**
   * Constructor por nombre y apellido
   * 
   * @param name
   * @param lastname
   */
  public MyPerson( String name, String lastname )
  {
    this.name = name;
    this.lastname = lastname;
  }

  /**
   * Constructor por nombre, apellido y fecha de nacimiento
   * 
   * @param name
   * @param lastname
   * @param birthday
   */
  public MyPerson( String name, String lastname, Date birthday )
  {
    this.name = name;
    this.lastname = lastname;
    this.birthday = birthday;
  }

  /**
   * Constructor por nombre, apellido, fecha de nacimiento y teléfono
   * 
   * @param name
   * @param lastname
   * @param birthday
   * @param phone
   */
  public MyPerson( String name, String lastname, Date birthday, String phone )
  {
    this.name = name;
    this.lastname = lastname;
    this.birthday = birthday;
    this.phone = phone;
  }

  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName( String name )
  {
    this.name = name;
  }

  /**
   * @return the lastname
   */
  public String getLastname()
  {
    return lastname;
  }

  /**
   * @param lastname the lastname to set
   */
  public void setLastname( String lastname )
  {
    this.lastname = lastname;
  }

  /**
   * @return the birthday
   */
  public Date getBirthday()
  {
    return birthday;
  }

  /**
   * @param birthday the birthday to set
   */
  public void setBirthday( Date birthday )
  {
    this.birthday = birthday;
  }

  /**
   * @return the phone
   */
  public String getPhone()
  {
    return phone;
  }

  /**
   * @param phone the phone to set
   */
  public void setPhone( String phone )
  {
    this.phone = phone;
  }

  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    if( this.name != null )
    {
      sb.append( "\"name\": \"" ).append( this.name ).append( "\"" );
    }

    if( this.lastname != null )
    {
      if( sb.length() > 0 )
      {
        sb.append( ", " );
      }

      sb.append( "\"lastname\": \"" ).append( this.lastname ).append( "\"" );
    }

    if( this.birthday != null )
    {
      if( sb.length() > 0 )
      {
        sb.append( ", " );
      }
      DateFormat df = new SimpleDateFormat( "yyyy-MM-dd" );
      sb.append( "\"birthday\": \"" ).append( df.format( birthday ) ).append( "\"" );
    }

    if( this.phone != null )
    {
      if( sb.length() > 0 )
      {
        sb.append( ", " );
      }
      sb.append( "\"name\": \"" ).append( this.name ).append( "\"" );
    }

    sb.insert( 0, "{ " );
    sb.append( " }" );

    return sb.toString();
  }

}
