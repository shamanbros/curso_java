package com.axity.course.java.session1.service.impl;

import java.util.Comparator;

import com.axity.course.java.session1.service.Sale;

public class SaleCountryComparator implements Comparator<Sale>
{

  private boolean asc;

  public SaleCountryComparator()
  {
    this.asc = true;
  }

  public SaleCountryComparator( boolean asc )
  {
    this.asc = asc;
  }

  @Override
  public int compare( Sale o1, Sale o2 )
  {
    int compare = 1;
    if( o1 != null && o1.getCountry() != null && o2 != null && o2.getCountry() != null )
    {
      if( asc )
      {
        compare = o1.getCountry().getData().compareTo( o2.getCountry().getData() );
      }
      else
      {
        compare = o2.getCountry().getData().compareTo( o1.getCountry().getData() );
      }

    }

    return compare;
  }

}
