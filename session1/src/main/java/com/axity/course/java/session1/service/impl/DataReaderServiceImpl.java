package com.axity.course.java.session1.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.axity.course.java.session1.service.DataReaderService;

public class DataReaderServiceImpl implements DataReaderService
{

  @Override
  public List<String> readData( String filename )
  {
    List<String> data = new ArrayList<>();

    InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream( filename );
    if( is != null )
    {

      BufferedReader rd = new BufferedReader( new InputStreamReader( is ) );

      StringBuilder result = new StringBuilder();
      try
      {
        String line = null;
        while( (line = rd.readLine()) != null )
        {
          data.add( line );
        }
      }
      catch( IOException e )
      {
        e.printStackTrace();
      }
    }

    return data;
  }

}
