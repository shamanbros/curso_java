package com.axity.course.java.session1.inheritance;

public enum TeacherType
{

  AUXILIAR, ASSISTANT, LECTURER, HEAD;
}
