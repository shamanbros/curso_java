package com.axity.course.java.session1.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.axity.course.java.session1.service.Catalog;
import com.axity.course.java.session1.service.CountryService;
import com.google.gson.Gson;

public class CountryServiceImpl implements CountryService
{

  @Override
  public List<Catalog> getCountries()
  {

    List<Catalog> countries = null;
    try
    {
      String url = "http://country.io/names.json";
      String data = getStringFromUrl( url );

      Gson gson = new Gson();

      Map<String, String> map = gson.fromJson( data, Map.class );

      countries = transform( map );
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }

    return countries;
  }

  private String getStringFromUrl( String url ) throws IOException
  {
    HttpClient client = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet( url );

    // add request header
    request.addHeader( "User-Agent",
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36" );
    HttpResponse response = client.execute( request );


    BufferedReader rd;
    String content = "";
    try
    {
      if( response.getEntity() != null && response.getEntity().getContent() != null )
      {
        rd = new BufferedReader( new InputStreamReader( response.getEntity().getContent() ) );
        content = readContent( rd );
      }
    }
    catch( UnsupportedOperationException | IOException e )
    {
      e.printStackTrace();
    }

    return content;

  }

  private String readContent( BufferedReader rd )
  {
    StringBuilder result = new StringBuilder();
    String line = "";
    try
    {
      while( (line = rd.readLine()) != null )
      {
        result.append( line );
      }
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }
    String content = result.toString();
    return content;
  }

  private List<Catalog> transform( Map<String, String> map )
  {
    List<Catalog> data = new ArrayList<>();

    for( Map.Entry<String, String> entry : map.entrySet() )
    {
      data.add( new Catalog( entry.getKey(), entry.getValue() ) );
    }

    return data;

  }

  @Override
  public List<Catalog> getPhoneCodes()
  {
    List<Catalog> phoneCodes = null;
    try
    {
      String url = "http://country.io/phone.json";
      String data = getStringFromUrl( url );
      Gson gson = new Gson();

      Map<String, String> map = gson.fromJson( data, Map.class );

      phoneCodes = transform( map );
    }
    catch( IOException e )
    {
      e.printStackTrace();
    }

    return phoneCodes;
  }
}
