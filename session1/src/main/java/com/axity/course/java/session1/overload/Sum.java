package com.axity.course.java.session1.overload;

public class Sum
{

  public int add( int a, int b )
  {
    return a + b;
  }

  public int add( int a, int b, int c )
  {
    return a + add( b, c );
  }

  public int add( int a, int b, int c, int d )
  {
    return a + add( b, c, d );
  }

  public int add( int a, int b, int c, int d, int e )
  {
    return a + add( b, c, d, e );
  }

}
