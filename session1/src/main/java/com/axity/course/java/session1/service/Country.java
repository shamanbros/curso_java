package com.axity.course.java.session1.service;

public class Country extends Catalog
{
  private String phoneCode;

  public Country()
  {
  }

  public Country( String code, String data )
  {
    super( code, data );
  }

  /**
   * @return the phoneCode
   */
  public String getPhoneCode()
  {
    return phoneCode;
  }

  /**
   * @param phoneCode the phoneCode to set
   */
  public void setPhoneCode( String phoneCode )
  {
    this.phoneCode = phoneCode;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "{ \"code\" : \"" ).append( this.code );
    sb.append( "\", \"data\" : \"" ).append( this.data );
    sb.append( ", \"phoneCode\" : \"" ).append( this.phoneCode );
    sb.append( "\" }" );
    return sb.toString();
  }
}
