package com.axity.course.java.session1.casting;

public class ABC extends AB
{

  public int c;

  @Override
  public String toString()
  {
    return new StringBuilder( "{ \"a\" : " ).append( this.a ).append( ", \"b\" : " ).append( this.b )
        .append( ", \"c\" : " ).append( this.c ).append( " }" ).toString();
  }
}
