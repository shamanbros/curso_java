package com.axity.course.java.session1.service.impl;

import java.util.Comparator;

import com.axity.course.java.session1.service.Sale;

public class SaleDateComparator implements Comparator<Sale>
{

  private boolean asc;

  public SaleDateComparator()
  {
    this.asc = true;
  }

  public SaleDateComparator( boolean asc )
  {
    this.asc = asc;
  }

  @Override
  public int compare( Sale o1, Sale o2 )
  {
    int compare = 1;
    if( o1 != null && o2 != null )
    {
      if( asc )
      {
        compare = o1.getDate().compareTo( o2.getDate() );
      }
      else
      {
        compare = o2.getDate().compareTo( o1.getDate() );
      }

    }

    return compare;
  }

}
