package com.axity.course.java.session1.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.axity.course.java.session1.service.Catalog;
import com.axity.course.java.session1.service.Country;
import com.axity.course.java.session1.service.CountryService;
import com.axity.course.java.session1.service.DataReaderService;
import com.axity.course.java.session1.service.Filter;
import com.axity.course.java.session1.service.Sale;
import com.axity.course.java.session1.service.SaleOrder;
import com.axity.course.java.session1.service.SalesService;

public class SalesServiceImpl implements SalesService
{

  private DataReaderService dataReaderService;
  private CountryService countryService;

  private Map<String, String> countryNames;
  private Map<String, String> countryPhoneCodes;
  private Map<String, Country> countries;

  @Override
  public List<Sale> findSales( Filter filter, SaleOrder saleOrder )
  {

    List<String> salesData = dataReaderService.readData( "data.txt" );

    List<Sale> sales = new ArrayList<>();
    for( String data : salesData )
    {
      Sale sale = createSale( data );
      sales.add( sale );
    }

    if( filter != null )
    {
      sales = applyFilter( sales, filter );
    }
    if( saleOrder != null )
    {
      applySaleOrder( sales, saleOrder );
    }

    return sales;
  }

  private Sale createSale( String data )
  {
    String[] saleRawData = data.split( "[\\|]" );
    String countryCode = saleRawData[0];
    String productCode = saleRawData[5];
    String productName = saleRawData[6];
    int sales = Integer.parseInt( saleRawData[4] );

    Date date = transformDate( saleRawData[1], saleRawData[2], saleRawData[3] );

    Sale sale = new Sale();
    Country country = getCountry( countryCode );
    sale.setCountry( country );
    sale.setProduct( new Catalog( productCode, productName ) );
    sale.setSales( sales );
    sale.setDate( date );

    return sale;
  }

  private Country getCountry( String countryCode )
  {
    if( this.countries == null )
    {
      this.countries = new HashMap<>();
    }
    Country country = null;
    if( this.countries.containsKey( countryCode ) )
    {
      country = this.countries.get( countryCode );
    }
    else
    {
      country = new Country( countryCode, getCountryName( countryCode ) );
      country.setPhoneCode( getCountryPhoneCode( countryCode ) );
      this.countries.put( countryCode, country );
    }

    return country;
  }

  private Date transformDate( String year, String month, String day )
  {
    Date date = null;
    DateFormat df = new SimpleDateFormat( "yyyy-m-d" );
    try
    {
      date = df.parse(
        new StringBuilder().append( year ).append( "-" ).append( month ).append( "-" ).append( day ).toString() );
    }
    catch( ParseException e )
    {
      e.printStackTrace();
    }

    return date;
  }

  private String getCountryName( String code )
  {
    if( this.countryNames == null )
    {
      this.countryNames = new HashMap<>();
      List<Catalog> countries = this.countryService.getCountries();
      for( Catalog country : countries )
      {
        this.countryNames.put( country.getCode(), country.getData() );
      }
    }
    return this.countryNames.get( code );
  }

  private String getCountryPhoneCode( String code )
  {
    if( this.countryPhoneCodes == null )
    {
      this.countryPhoneCodes = new HashMap<>();
      List<Catalog> countries = this.countryService.getPhoneCodes();
      for( Catalog country : countries )
      {
        this.countryPhoneCodes.put( country.getCode(), country.getData() );
      }
    }
    return this.countryPhoneCodes.get( code );
  }

  private void applySaleOrder( List<Sale> salesData, SaleOrder saleOrder )
  {

    switch( saleOrder )
    {
      case COUNTRY_ASC:
        Collections.sort( salesData, new SaleCountryComparator( true ) );
        break;
      case COUNTRY_DESC:
        Collections.sort( salesData, new SaleCountryComparator( false ) );
        break;
      case DATE_ASC:
        Collections.sort( salesData, new SaleDateComparator( true ) );
        break;
      case DATE_DESC:
        Collections.sort( salesData, new SaleDateComparator( false ) );
        break;
      case PRODUCT_ASC:
        Collections.sort( salesData, new SaleProductComparator( true ) );
        break;
      case PRODUCT_DESC:
        Collections.sort( salesData, new SaleProductComparator( false ) );
        break;
      case SALES_ASC:
        Collections.sort( salesData, new SaleSalesComparator( true ) );
        break;
      case SALES_DESC:
        Collections.sort( salesData, new SaleSalesComparator( false ) );
        break;
      default:
        // undefined
        throw new RuntimeException( "Invalid sorting" );
    }

  }

  private List<Sale> applyFilter( List<Sale> sales, Filter filter )
  {

    List<Sale> filtered = new ArrayList<>();

    for( Sale sale : sales )
    {

      if( filter.getType().equals( Filter.Type.COUNTRY ) && sale.getCountry().getCode().equals( filter.getData() ) )
      {
        filtered.add( sale );
      }
      else if( filter.getType().equals( Filter.Type.PRODUCT )
          && sale.getProduct().getCode().equals( filter.getData() ) )
      {
        filtered.add( sale );
      }
      else if( filter.getType().equals( Filter.Type.DATE ) && isSameDate( sale.getDate(), filter.getData() ) )
      {
        filtered.add( sale );
      }
      else if( filter.getType().equals( Filter.Type.SALES_GTE )
          && Integer.parseInt( filter.getData() ) >= sale.getSales() )
      {
        filtered.add( sale );
      }
      else if( filter.getType().equals( Filter.Type.SALES_LTE )
          && Integer.parseInt( filter.getData() ) <= sale.getSales() )
      {
        filtered.add( sale );
      }

    }

    return filtered;
  }

  private boolean isSameDate( Date date, String data )
  {
    boolean isSameDate = false;

    if( date != null )
    {
      DateFormat df = new SimpleDateFormat( "yyyy-mm-dd" );
      try
      {
        Calendar calDate = Calendar.getInstance();
        calDate.setTime( date );

        Date filter = df.parse( data );
        Calendar calFilter = Calendar.getInstance();
        calFilter.setTime( filter );

        isSameDate = calDate.get( Calendar.YEAR ) == calFilter.get( Calendar.YEAR )
            && calDate.get( Calendar.MONTH ) == calFilter.get( Calendar.MONTH )
            && calDate.get( Calendar.DATE ) == calFilter.get( Calendar.DATE );

      }
      catch( ParseException e )
      {
        e.printStackTrace();
      }

    }

    return isSameDate;
  }

  /**
   * @param dataReaderService the dataReaderService to set
   */
  public void setDataReaderService( DataReaderService dataReaderService )
  {
    this.dataReaderService = dataReaderService;
  }

  /**
   * @param countryService the countryService to set
   */
  public void setCountryService( CountryService countryService )
  {
    this.countryService = countryService;
  }

}
