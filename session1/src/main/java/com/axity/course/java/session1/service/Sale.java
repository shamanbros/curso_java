package com.axity.course.java.session1.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Sale implements Comparable<Sale>
{

  private Country country;
  private Catalog product;
  private Date date;
  private int sales;

  /**
   * @return the country
   */
  public Country getCountry()
  {
    return country;
  }

  /**
   * @param country the country to set
   */
  public void setCountry( Country country )
  {
    this.country = country;
  }

  /**
   * @return the product
   */
  public Catalog getProduct()
  {
    return product;
  }

  /**
   * @param product the product to set
   */
  public void setProduct( Catalog product )
  {
    this.product = product;
  }

  /**
   * @return the date
   */
  public Date getDate()
  {
    return date;
  }

  /**
   * @param date the date to set
   */
  public void setDate( Date date )
  {
    this.date = date;
  }

  /**
   * @return the sales
   */
  public int getSales()
  {
    return sales;
  }

  /**
   * @param sales the sales to set
   */
  public void setSales( int sales )
  {
    this.sales = sales;
  }

  @Override
  public String toString()
  {

    DateFormat df = new SimpleDateFormat( "yyyy-mm-dd" );
    String dateString = null;
    if( this.date != null )
    {
      dateString = df.format( this.date );
    }

    StringBuilder sb = new StringBuilder();

    sb.append( "{ \"country\" : " ).append( this.country );
    sb.append( ", \"product\" : " ).append( this.product );
    sb.append( ", \"date\" : \"" ).append( dateString );

    sb.append( "\", \"sales\" : " ).append( this.sales ).append( " }" );

    return sb.toString();

  }

  @Override
  public boolean equals( Object object )
  {
    boolean isEquals;
    if( this == object )
    {
      isEquals = true;
    }
    else if( object == null )
    {
      isEquals = false;
    }
    else if( object instanceof Sale )
    {

      Sale that = (Sale) object;
      isEquals = that.country != null && that.country.code != null && this.country.code.equals( that.country.code );
      isEquals &= that.product != null && that.product.code != null && this.product.code.equals( that.product.code );
      isEquals &= that.date != null && this.date.equals( that.date );
    }
    else
    {
      isEquals = false;
    }

    return isEquals;
  }

  @Override
  public int hashCode()
  {
    int acum = 0;
    int p1 = 17;
    int p2 = 37;

    acum += p1 + p2;
    if( this.country != null && this.country.code != null )
    {
      acum += p1 + (this.country.code.hashCode() * p2);
    }

    if( this.product != null && this.product.code != null )
    {
      acum += p1 + (this.product.code.hashCode() * p2);
    }

    if( this.date != null )
    {
      acum += p1 + (this.date.hashCode() * p2);
    }

    return acum;
  }

  @Override
  public int compareTo( Sale that )
  {

    // String s = new StringBuilder().append( country.code ).append( "," ).append( this.product.code ).append( "," )
    // .append( date.toString() ).toString();
    //
    // String s2 = new StringBuilder().append( that.country.code )
    // .append( "," ).append( that.product.code ).append( "," )
    // .append(that. date.toString() ).toString();

    int compare = 1;

    if( this.country != null )
    {
      if( that.country != null && that.country.code != null )
      {
        compare = this.country.code.compareTo( that.country.code );

        if( compare == 0 )
        {

          if( this.product != null )
          {
            if( that.product != null && that.product.code != null )
            {
              compare = this.product.code.compareTo( that.product.code );

              if( compare == 0 )
              {
                if( this.date != null )
                {
                  if( that.date != null )
                  {
                    compare = this.date.compareTo( that.date );
                  }
                }
                else
                {
                  compare = -1;
                }
              }
            }
          }
          else
          {
            compare = -1;
          }
        }
      }

    }
    else
    {
      compare = -1;
    }

    return compare;
  }
}
