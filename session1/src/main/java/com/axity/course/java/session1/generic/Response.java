package com.axity.course.java.session1.generic;

public class Response
{
  private Header header;
  private Body body;

  /**
   * @return the header
   */
  public Header getHeader()
  {
    return header;
  }

  /**
   * @param header the header to set
   */
  public void setHeader( Header header )
  {
    this.header = header;
  }

  /**
   * @return the body
   */
  public Body getBody()
  {
    return body;
  }

  /**
   * @param body the body to set
   */
  public void setBody( Body body )
  {
    this.body = body;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "[ header: " ).append( header );
    sb.append( ", body: " ).append( body ).append( "]" );

    return sb.toString();
  }
}
