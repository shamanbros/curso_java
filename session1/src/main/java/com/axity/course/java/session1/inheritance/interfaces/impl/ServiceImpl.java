package com.axity.course.java.session1.inheritance.interfaces.impl;

import com.axity.course.java.session1.inheritance.interfaces.Service;

public class ServiceImpl implements Service
{

  @Override
  public void sing()
  {
    System.out
        .println( "Doe, a deer, a female deer\r\n" + "Ray, a drop of golden sun\r\n" + "Me, a name I call myself\r\n"
            + "Far, a long, long way to run\r\n" + "Sew, a needle pulling thread\r\n" + "La, a note to follow Sew\r\n"
            + "Tea, a drink with jam and bread\r\n" + "That will bring us back to Do (oh-oh-oh)" );

  }

  @Override
  public void serve( int id )
  {
    System.out.println( "Serving order " + id );

  }

}
