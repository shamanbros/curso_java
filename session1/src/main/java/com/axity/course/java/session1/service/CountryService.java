package com.axity.course.java.session1.service;

import java.util.List;

public interface CountryService
{
  List<Catalog> getCountries();

  List<Catalog> getPhoneCodes();
}
