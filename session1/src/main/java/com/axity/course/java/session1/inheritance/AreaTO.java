package com.axity.course.java.session1.inheritance;

import java.util.List;

/**
 * Clase de las areas
 * 
 * @author gsegura
 */
public class AreaTO extends CatalogTO
{

  private static final long serialVersionUID = -1186518628415998530L;
  private PersonTO manager;
  private PersonTO secretary;
  private List<PersonTO> members;

  /**
   * Constructor default
   */
  public AreaTO()
  {
    super();
  }

  /**
   * Constructor por id
   * 
   * @param id
   */
  public AreaTO( Long id )
  {
    super( id );
  }

  /**
   * Constructor por nombre
   * 
   * @param name
   */
  public AreaTO( String name )
  {
    super( name );
  }

  /**
   * Constructor por id y nombre
   * 
   * @param id
   * @param name
   */
  public AreaTO( Long id, String name )
  {
    super( id, name );
  }

  /**
   * @return the manager
   */
  public PersonTO getManager()
  {
    return manager;
  }

  /**
   * @param manager the manager to set
   */
  public void setManager( PersonTO manager )
  {
    this.manager = manager;
  }

  /**
   * @return the secretary
   */
  public PersonTO getSecretary()
  {
    return secretary;
  }

  /**
   * @param secretary the secretary to set
   */
  public void setSecretary( PersonTO secretary )
  {
    this.secretary = secretary;
  }

  /**
   * @return the members
   */
  public List<PersonTO> getMembers()
  {
    return members;
  }

  /**
   * @param members the members to set
   */
  public void setMembers( List<PersonTO> members )
  {
    this.members = members;
  }

}
