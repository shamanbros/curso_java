package com.axity.course.java.session1.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.axity.course.java.session1.service.impl.CountryServiceImpl;
import com.axity.course.java.session1.service.impl.DataReaderServiceImpl;
import com.axity.course.java.session1.service.impl.SalesServiceImpl;

public class SalesServiceTest
{
  private SalesService salesService;

  @Before
  public void setUp()
  {
    salesService = new SalesServiceImpl();
    ((SalesServiceImpl) salesService).setCountryService( new CountryServiceImpl() );
    ((SalesServiceImpl) salesService).setDataReaderService( new DataReaderServiceImpl() );
  }

  @Test
  public void testFindSales()
  {
    List<Sale> sales = this.salesService.findSales( null, null );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_filterCountry()
  {

    Filter filter = new Filter();
    filter.setData( "MX" );
    filter.setType( Filter.Type.COUNTRY );

    List<Sale> sales = this.salesService.findSales( filter, null );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_filterProduct()
  {

    Filter filter = new Filter();
    filter.setData( "U0003" );
    filter.setType( Filter.Type.PRODUCT );

    List<Sale> sales = this.salesService.findSales( filter, null );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_filterDate()
  {

    Filter filter = new Filter();
    filter.setData( "2019-01-05" );
    filter.setType( Filter.Type.DATE );

    List<Sale> sales = this.salesService.findSales( filter, null );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_filterSalesGTE()
  {

    Filter filter = new Filter();
    filter.setData( "50" );
    filter.setType( Filter.Type.SALES_GTE );

    List<Sale> sales = this.salesService.findSales( filter, null );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_filterSalesLTE()
  {

    Filter filter = new Filter();
    filter.setData( "50" );
    filter.setType( Filter.Type.SALES_LTE );

    List<Sale> sales = this.salesService.findSales( filter, null );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_COUNTRY_ASC()
  {

    List<Sale> sales = this.salesService.findSales( null, SaleOrder.COUNTRY_ASC );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_COUNTRY_DESC()
  {

    List<Sale> sales = this.salesService.findSales( null, SaleOrder.COUNTRY_DESC );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_DATE_ASC()
  {

    List<Sale> sales = this.salesService.findSales( null, SaleOrder.DATE_ASC );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_DATE_DESC()
  {

    List<Sale> sales = this.salesService.findSales( null, SaleOrder.DATE_DESC );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_PRODUCT_ASC()
  {

    List<Sale> sales = this.salesService.findSales( null, SaleOrder.PRODUCT_ASC );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_PRODUCT_DESC()
  {

    List<Sale> sales = this.salesService.findSales( null, SaleOrder.PRODUCT_DESC );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_SALES_ASC()
  {

    List<Sale> sales = this.salesService.findSales( null, SaleOrder.SALES_ASC );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_SALES_DESC()
  {

    List<Sale> sales = this.salesService.findSales( null, SaleOrder.SALES_DESC );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testFindSales_filterCountry_SALES_ASC()
  {
    Filter filter = new Filter();
    filter.setData( "MX" );
    filter.setType( Filter.Type.COUNTRY );

    List<Sale> sales = this.salesService.findSales( filter, SaleOrder.SALES_ASC );
    Assert.assertNotNull( sales );
    Assert.assertFalse( sales.isEmpty() );
    for( Sale sale : sales )
    {
      System.out.println( sale );
    }
  }

  @Test
  public void testEquals()
  {
    Object o1 = new Object();
    Object o2 = new Object();

    Object o3 = o1;

    System.out.println( o1.equals( o2 ) );
    System.out.println( o1.equals( o3 ) );

    System.out.println( o1.equals( null ) );

    Object o4 = null;

    System.out.println( o1.equals( o4 ) );
  }

  @Test
  public void testEqualsSale()
  {
    Sale s1 = new Sale();
    s1.setCountry( new Country( "AR", "Argentina" ) );
    s1.setProduct( new Catalog( "U001", "Item 1" ) );
    s1.setDate( createDate( "2019-01-01" ) );

    Sale s2 = new Sale();
    s2.setCountry( new Country( "AR", "Argentina" ) );
    s2.setProduct( new Catalog( "U001", "Item 1" ) );
    s2.setDate( createDate( "2019-01-01" ) );

    Sale s3 = s1;

    System.out.println( s1.equals( s2 ) );
    System.out.println( s1.equals( s3 ) );
    System.out.println( s1.equals( null ) );
  }

  @Test
  public void testEqualsSale2()
  {
    Sale s1 = new Sale();
    s1.setCountry( new Country( "AR", "Argentina" ) );
    s1.setProduct( new Catalog( "U001", "Item 1" ) );
    s1.setDate( createDate( "2019-01-01" ) );

    Sale s2 = new Sale();
    // s2.setCountry( new Country( "AR", "Argentina" ) );
    s2.setProduct( new Catalog( "U001", "Item 1" ) );
    s2.setDate( createDate( "2019-01-01" ) );

    Sale s3 = s1;

    System.out.println( s1.equals( s2 ) );
    System.out.println( s1.equals( s3 ) );
    System.out.println( s1.equals( null ) );
  }

  @Test
  public void testEqualsSale3()
  {
    Sale s1 = new Sale();
    s1.setCountry( new Country( "AR", "Argentina" ) );
    s1.setProduct( new Catalog( "U001", "Item 1" ) );
    s1.setDate( createDate( "2019-01-01" ) );

    Sale s2 = new Sale();
    s2.setCountry( new Country( "AR", "Argentina" ) );
    s2.setProduct( new Catalog( "U001", "Item 1" ) );
    s2.setDate( createDate( "2019-01-01" ) );

    Sale s3 = new Sale();
    s3.setCountry( new Country( "AR", "Argentina" ) );
    s3.setProduct( new Catalog( "U002", "Item 1" ) );
    s3.setDate( createDate( "2019-01-01" ) );

    System.out.println( s1.equals( s2 ) );
    System.out.println( s1.hashCode() );
    System.out.println( s2.hashCode() );

    System.out.println( s1.equals( s3 ) );
    System.out.println( s1.hashCode() );
    System.out.println( s3.hashCode() );

  }

  @Test
  public void testCompareTo()
  {
    Sale s1 = new Sale();
    s1.setCountry( new Country( "AR", "Argentina" ) );
    s1.setProduct( new Catalog( "U001", "Item 1" ) );
    s1.setDate( createDate( "2019-01-01" ) );

    Sale s2 = new Sale();
    s2.setCountry( new Country( "AR", "Argentina" ) );
    s2.setProduct( new Catalog( "U001", "Item 1" ) );
    s2.setDate( createDate( "2019-01-01" ) );

    Sale s3 = new Sale();
    s3.setCountry( new Country( "AR", "Argentina" ) );
    s3.setProduct( new Catalog( "U002", "Item 1" ) );
    s3.setDate( createDate( "2019-01-01" ));
    
    
    System.out.println( s1.compareTo( s2 ) );
    System.out.println( s1.compareTo( s3 ) );
  }

  private Date createDate( String string )
  {
    DateFormat df = new SimpleDateFormat( "yyyy-mm-dd" );
    Date date = null;
    try
    {
      date = df.parse( string );
    }
    catch( ParseException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return date;
  }

}
