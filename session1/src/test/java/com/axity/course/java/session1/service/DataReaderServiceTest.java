package com.axity.course.java.session1.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.axity.course.java.session1.service.impl.DataReaderServiceImpl;

public class DataReaderServiceTest
{

  private DataReaderService dataReaderService;

  @Before
  public void setUp()
  {
    dataReaderService = new DataReaderServiceImpl();
  }

  @Test
  public void testReadData()
  {
    List<String> data = dataReaderService.readData( "data.txt" );
    Assert.assertNotNull( data );
    Assert.assertFalse( data.isEmpty() );
    for( String s : data )
    {
      System.out.println( s );
    }
  }

}
