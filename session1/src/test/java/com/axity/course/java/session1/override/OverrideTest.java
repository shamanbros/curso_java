package com.axity.course.java.session1.override;

import org.junit.Test;

public class OverrideTest
{

  @Test
  public void test()
  {
    A a = new A();

    System.out.println( a );

  }

  @Test
  public void test2()
  {
    A a = new MyA();

    System.out.println( a );

  }

  @Test
  public void test3()
  {
    MyA a = new MyAnotherA();

    System.out.println( a );

  }

  @Test
  public void test4()
  {
    AbstractLetter a = new MyAnotherA();

    System.out.println( ((A) a).toString() );

    a = new A();
    System.out.println( a );
    
    a = new MyA();
    System.out.println( a );
  }

}
