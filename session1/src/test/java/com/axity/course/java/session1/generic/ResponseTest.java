package com.axity.course.java.session1.generic;

import static org.junit.Assert.*;

import org.junit.Test;

public class ResponseTest
{

  @Test
  public void test()
  {
    Response response = new Response();
    Body body = new Body();
    body.setA( 10 );
    response.setBody( body );
    Header header = new Header();
    header.setId( 10 );
    header.setCode( "200" );
    response.setHeader( header );
    
    System.out.println( response );
  }
  
  @Test
  public void testOtherBody()
  {
    Response response = new Response();
    OtherBody body = new OtherBody();
    body.setA( 10 );
    body.setB( 20 );
    response.setBody( body );
    Header header = new Header();
    header.setId( 10 );
    header.setCode( "200" );
    response.setHeader( header );
    
    System.out.println( response );
  }
  
  @Test
  public void testErrorCode()
  {
    Response response = new Response();
    
    Header header = new ErrorHeader("E0001", "An error has occured!!!");
    header.setId( 10 );
    header.setCode( "200" );    
    response.setHeader( header );
    
    System.out.println( response );
  }

}
