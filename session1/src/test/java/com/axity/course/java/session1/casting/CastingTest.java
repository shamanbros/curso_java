package com.axity.course.java.session1.casting;

import org.junit.Test;

public class CastingTest
{

  @Test
  public void test()
  {

    AbstractLetter abc = new ABC();
    ((A) abc).a = 1;
    ((AB) abc).b = 2;
    ((ABC) abc).c = 3;

    System.out.println( abc );

  }

  @Test
  public void test2()
  {

    A ab = new AB();
    ab.a = 10;

    System.out.println( (AB) ab );

  }

  @Test
  public void test3()
  {

    A abc = new ABC();
    ((ABC) abc).a = 7;
    ((ABC) abc).b = 11;
    ((ABC) abc).c = 13;

    System.out.println( abc );

  }

  @Test
  public void test4()
  {

    // A a1 = new A();
    // setData( 10, 15, 20, a1 );
    // System.out.println( a1 );
    //
    // A a2 = new AB();
    // setData( 10, 15, 20, a2 );
    // System.out.println( a2 );

    // Falla por qué?
    A a3 = new ABC();
    setData( 10, 15, 20, a3 );
    System.out.println( a3 );

  }

  @Test
  public void testInstanceOfA()
  {

    System.out.println( new A() instanceof A );
    System.out.println( new AB() instanceof A );
    System.out.println( new ABC() instanceof A );
    
    System.out.println( new A().getClass().equals( new A().getClass() ) );
  }

  @Test
  public void testInstanceOfAB()
  {
    System.out.println( new A() instanceof AB );
    System.out.println( new AB() instanceof AB );
    System.out.println( new ABC() instanceof AB );

  }

  @Test
  public void testInstanceOfABC()
  {
    System.out.println( new A() instanceof ABC );
    System.out.println( new AB() instanceof ABC );
    System.out.println( new ABC() instanceof ABC );

  }

  private void setData( int a, int b, int c, A myA )
  {
    myA.a = a;
    if( myA instanceof ABC )
    {
      ((ABC) myA).c = c;
    }
    else if( myA instanceof AB )
    {
      ((AB) myA).b = b;
    }

  }

}
