package com.axity.course.java.session1.overload;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class SumTest
{
  private Sum sum;

  @Before
  public void setUp()
  {
    sum = new Sum();
  }

  @Test
  public void testAddIntInt()
  {
    System.out.println( sum.add( 5, 10 ) );
  }

  @Test
  public void testAddIntIntInt()
  {
    System.out.println( sum.add( 5, 10, 200 ) );
  }

  @Test
  public void testAddIntIntIntInt()
  {
    System.out.println( sum.add( 5, 10, 200, 5000 ) );
  }

  @Test
  public void testAddIntIntIntIntInt()
  {
    System.out.println( sum.add( 5, 10, 200, 5000, 10000 ) );
  }

}
