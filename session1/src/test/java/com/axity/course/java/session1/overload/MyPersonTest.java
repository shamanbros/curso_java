package com.axity.course.java.session1.overload;

import java.util.Calendar;

import org.junit.Test;

public class MyPersonTest
{

  @Test
  public void testDefault()
  {
    System.out.println( new MyPerson() );
  }
  
  @Test
  public void testDefault2()
  {
    System.out.println( new MyPerson("Pedro") );
  }
  
  @Test
  public void testDefault3()
  {
    System.out.println( new MyPerson("Pedro", "Arteaga") );
  }
  
  @Test
  public void testDefault4()
  {
    Calendar cal = Calendar.getInstance();
    cal.set( 1995, Calendar.MAY, 15 );
    
    System.out.println( new MyPerson("Pedro", "Arteaga", cal.getTime() ) );
  }
  
  @Test
  public void testDefault5()
  {
    Calendar cal = Calendar.getInstance();
    cal.set( 1995, Calendar.MAY, 15 );
    
    System.out.println( new MyPerson("Pedro", "Arteaga", cal.getTime(), "5658 1111" ) );
  }
}
