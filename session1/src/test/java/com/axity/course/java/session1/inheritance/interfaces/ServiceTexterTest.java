package com.axity.course.java.session1.inheritance.interfaces;

import org.junit.Before;
import org.junit.Test;

import com.axity.course.java.session1.inheritance.interfaces.impl.DuHast;
import com.axity.course.java.session1.inheritance.interfaces.impl.DustInTheWind;
import com.axity.course.java.session1.inheritance.interfaces.impl.RobocopServiceImpl;
import com.axity.course.java.session1.inheritance.interfaces.impl.ServiceImpl;
import com.axity.course.java.session1.inheritance.interfaces.impl.ServiceTexterImpl;

public class ServiceTexterTest
{
  private ServiceTexter serviceTexter;

  @Before
  public void setUp()
  {
    serviceTexter = new ServiceTexterImpl();
  }

  @Test
  public void testServe()
  {
    serviceTexter.serve( 1 );
  }

  @Test
  public void testServeSetService()
  {
    RobocopServiceImpl robocop = new RobocopServiceImpl();
    serviceTexter.setService( robocop );
    serviceTexter.setSinger( robocop );
    serviceTexter.serve( 1 );
    serviceTexter.serve( 2 );
    serviceTexter.serve( 3 );
    serviceTexter.serve( 4 );
    serviceTexter.serve( 5 );

    try
    {
      serviceTexter.sing();
    }
    catch( RuntimeException e )
    {
      System.err.println( e.getMessage() );
    }
  }

  @Test
  public void testServeSetService2()
  {
    RobocopServiceImpl robocop = new RobocopServiceImpl( new DuHast() );

    serviceTexter.setService( robocop );
    serviceTexter.setSinger( robocop );
    serviceTexter.serve( 1 );
    serviceTexter.serve( 5 );

    serviceTexter.sing();

  }
  
  @Test
  public void testServeSetService3()
  {
    RobocopServiceImpl robocop = new RobocopServiceImpl( new DustInTheWind() );

    serviceTexter.setService( robocop );
    serviceTexter.setSinger( robocop );
    serviceTexter.serve( 1 );
    serviceTexter.serve( 5 );

    serviceTexter.sing();

  }

  @Test
  public void testSing()
  {
    serviceTexter.sing();
  }

  @Test
  public void testSetSinger()
  {
    serviceTexter.setSinger( new DustInTheWind() );
    serviceTexter.sing();
  }

  @Test
  public void testSetSinger2()
  {
    serviceTexter.setSinger( new ServiceImpl() );
    serviceTexter.sing();
  }

  @Test
  public void testText()
  {
    serviceTexter.text( "qwerty" );
  }

  @Test
  public void testSayHello()
  {
    serviceTexter.sayHello( "Maria" );
  }

}
