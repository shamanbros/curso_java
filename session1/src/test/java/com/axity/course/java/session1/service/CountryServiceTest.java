package com.axity.course.java.session1.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.axity.course.java.session1.service.impl.CountryServiceImpl;

public class CountryServiceTest
{
  private CountryService countryService;

  @Before
  public void setUp()
  {
    countryService = new CountryServiceImpl();
  }

  @Test
  public void testGetCountries()
  {
    List<Catalog> countries = countryService.getCountries();
    Assert.assertNotNull( countries );
    Assert.assertFalse( countries.isEmpty() );
    for( Catalog c : countries )
    {
      System.out.println( c );
    }
  }

  @Test
  public void testGetPhoneCodes()
  {
    List<Catalog> phoneCodes = countryService.getPhoneCodes();
    Assert.assertNotNull( phoneCodes );
    Assert.assertFalse( phoneCodes.isEmpty() );
    for( Catalog c : phoneCodes )
    {
      System.out.println( c );
    }
  }

}
