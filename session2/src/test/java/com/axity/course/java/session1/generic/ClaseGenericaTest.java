package com.axity.course.java.session1.generic;

import static org.junit.Assert.fail;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.axity.course.java.session2.generic.ClaseGenerica;

public class ClaseGenericaTest
{

  @Test
  public void testImprimeListaString()
  {
    List<String> list = new ArrayList<>();
    list.add( "ABC" );
    list.add( "QWERTY" );
    list.add( "123" );
    list.add( "abcdefg" );

    ClaseGenerica claseGenerica = new ClaseGenerica();

    claseGenerica.imprimeListaString( list );
  }

  @Test
  public void testImprimeListaNumeric()
  {
    List<Number> list = new ArrayList<Number>();
    list.add( 10 );
    list.add( 10L );
    list.add( 12.1 );
    list.add( new BigInteger( "100" ) );

    ClaseGenerica claseGenerica = new ClaseGenerica();

    claseGenerica.imprimeListaNumeric( list );
  }

}
