package com.axity.course.java.session1.generic;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.axity.course.java.session2.generic.ClaseNoGenerica;

public class ClaseNoGenericaTest
{

  @Test
  public void testImprimeLista_string()
  {
    List list = new ArrayList();
    list.add( "ABC" );
    list.add( "QWERTY" );
    list.add( "123" );
    list.add( "abcdefg" );
    
    ClaseNoGenerica  claseNoGenerica = new ClaseNoGenerica();
    
    claseNoGenerica.imprimeLista( list );
  }
  
  @Test
  public void testImprimeLista_number()
  {
    List list = new ArrayList();
    list.add( 10 );
    list.add( 10L);
    list.add( 12.1 );
    list.add( new BigInteger( "100" ) );
    
    ClaseNoGenerica  claseNoGenerica = new ClaseNoGenerica();
    
    claseNoGenerica.imprimeLista( list );
  }

}
