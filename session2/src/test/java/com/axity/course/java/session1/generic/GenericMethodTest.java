package com.axity.course.java.session1.generic;

import static org.junit.Assert.*;

import org.junit.Test;

import com.axity.course.java.session2.generic.GenericMethod;
import com.axity.course.java.session2.generic.GenericsType;

public class GenericMethodTest
{

  @Test
  public void testIsEqual()
  {
    GenericsType<Integer> g1 = new GenericsType<Integer>( 1 );
    GenericsType<Integer> g2 = new GenericsType<Integer>( 1 );

    boolean isEquals = GenericMethod.isEqual( g1, g2 );
    System.out.println( isEquals );
  }

  @Test
  public void testIsEqual_Integer()
  {
    boolean isEquals = GenericMethod.isEqual( 1, 1 );
    System.out.println( isEquals );
  }
  
  @Test
  public void testIsEqual_String()
  {
    boolean isEquals = GenericMethod.isEqual( "ABC", "ABC" );
    System.out.println( isEquals );
  }
}
