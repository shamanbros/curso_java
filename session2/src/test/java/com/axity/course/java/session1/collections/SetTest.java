package com.axity.course.java.session1.collections;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

public class SetTest
{

  @Test
  public void testHashSet()
  {

    int[] data = { 5, 3, 11, 7, 13, 1, 19, 23, 2, 17, 31 };

    Set<Integer> set = new HashSet<>();
    for( int n : data )
    {
      set.add( n );
      System.out.println( Arrays.toString( set.toArray() ) );
    }

  }

  @Test
  public void testTreeSet()
  {
    int[] data = { 5, 3, 11, 7, 13, 1, 19, 23, 2, 17, 31 };
    Set<Integer> set = new TreeSet<>();
    for( int n : data )
    {
      set.add( n );
      System.out.println( Arrays.toString( set.toArray() ) );
    }

  }

  @Test
  public void testLinkedSet()
  {
    int[] data = { 5, 3, 11, 7, 13, 1, 19, 23, 2, 17, 31 };
    Set<Integer> set = new LinkedHashSet<>();
    for( int n : data )
    {
      set.add( n );
      System.out.println( Arrays.toString( set.toArray() ) );
    }

  }

  @Test
  public void testUnmodifiable()
  {
    int[] data = { 5, 3, 11, 7, 13, 1, 19, 23, 2, 17, 31 };
    Set<Integer> set = new LinkedHashSet<>();
    for( int n : data )
    {
      set.add( n );
    }

    Set<Integer> unmodifiedSet = Collections.unmodifiableSet( set );

    unmodifiedSet.add( 5 );
  }

  @Test
  public void testList()
  {

    List<Integer> list = new ArrayList<>( Collections.nCopies( 10, 1 ) );
    for( int i : list )
    {
      System.out.print( i + " " );
    }
    System.out.println();
    Collections.fill( list, 3 );

    for( int i : list )
    {
      System.out.print( i + " " );
    }
  }

  @Test
  public void testDisjoint()
  {
    List<Integer> list1 = new ArrayList<>(Arrays.asList( 1, 2, 3, 4, 5, 6, 7, 8 ));
    List<Integer> list2 = new ArrayList<>(Arrays.asList( 5, 6, 9, 10 ));
    
    List<Integer> other = new ArrayList<>(list1);
    other.removeAll( list2 );
    
    for( int i : list1 )
    {
       System.out.print(  i + " " );
    }
    System.out.println();
    for( int i : other )
    {
      System.out.print( i + " " );
    }
    
    
  }

}
