package com.axity.course.java.session1.generic;

import static org.junit.Assert.*;

import org.junit.Test;

import com.axity.course.java.session2.generic.GenericsType;
import com.axity.course.java.session2.generic.NonGenericsType;

public class GenericsTypeTest
{

  @Test
  public void testNonGeneric()
  {
    NonGenericsType nonGenericsType = new NonGenericsType();
    nonGenericsType.set( "QWERTY" );

    Object o = nonGenericsType.get();

    System.out.println( (String) o );

  }

  @Test
  public void testGeneric()
  {
    GenericsType<String> genericsType = new GenericsType<String>();
    genericsType.set( "QWERTY" );

    String s = genericsType.get();

    System.out.println( s );

  }
  
  @Test
  public void testGeneric_v2()
  {
    GenericsType<Number> genericsType = new GenericsType<Number>();
    genericsType.set( 100.0 );

    Number n = genericsType.get();

    System.out.println( n );

  }

}
