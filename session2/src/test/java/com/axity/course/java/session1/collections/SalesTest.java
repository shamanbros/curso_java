package com.axity.course.java.session1.collections;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.axity.course.java.session2.exercise.Sale;
import com.axity.course.java.session2.exercise.SaleService;
import com.axity.course.java.session2.exercise.SaleServiceImpl;

public class SalesTest
{

  private SaleService saleService;
  private List<Sale> sales;

  @Before
  public void setUp()
  {
    saleService = new SaleServiceImpl();
    sales = saleService.getSales();
  }

  @Test
  public void test()
  {

    DateFormat df = new SimpleDateFormat( "yyyy-mm-dd" );

    for( Sale sale : this.sales )
    {

      System.out.println( sale.getCountry() + "\t" + sale.getStore() + "\t" + sale.getZone() + "\t"
          + df.format( sale.getDate() ) + "\t" + sale.getSale() );
    }

  }

}
