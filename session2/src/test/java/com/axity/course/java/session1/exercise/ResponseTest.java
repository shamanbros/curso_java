package com.axity.course.java.session1.exercise;

import org.junit.Test;

import com.axity.course.java.session2.exercise.Body;
import com.axity.course.java.session2.exercise.ErrorHeader;
import com.axity.course.java.session2.exercise.Header;
import com.axity.course.java.session2.exercise.OtherBody;
import com.axity.course.java.session2.exercise.Response;
import com.google.gson.Gson;

public class ResponseTest
{

  @Test
  public void test()
  {
    Response response = new Response();
    
    Body body = new Body();
    body.setA( 10 );
    response.setBody( body  );
    
    Header header = new Header();
    header.setCode( "0" );
    header.setId( 100 );
    response.setHeader( header  );
    
    Gson gson = new Gson();
    
    System.out.println( gson.toJson( response ) );
    
  }
  
  
  @Test
  public void test2()
  {
    Response response = new Response();
    
    OtherBody body = new OtherBody();
    body.setB( 11 );
    response.setBody( body  );
    
    ErrorHeader header = new ErrorHeader();
//    header.setCode( "0" );
//    header.setId( 100 );
    header.setDescription( "Ocurrió un error" );
    header.setErrorCode( "E0001" );
    response.setHeader( header  );
    
    Gson gson = new Gson();
    
    System.out.println( gson.toJson( response ) );
    
  }

}
