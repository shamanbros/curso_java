package com.axity.course.java.session1.generic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.axity.course.java.session2.generic.BoundedGeneric;

public class BoundedGenericTest
{

  @Test
  public void testSumBounded()
  {
    BoundedGeneric boundedGeneric = new BoundedGeneric();

    List<Number> list = new ArrayList<>();
    list.add( 10.0 );
    list.add( 10 );
    list.add( 10L );
    list.add( new BigDecimal( "10" ) );
    double n = boundedGeneric.sumBounded( list );
    System.out.println( "sum = " + n );
  }

  public void testSumBounded_error()
  {
    BoundedGeneric boundedGeneric = new BoundedGeneric();

    List<Integer> list = new ArrayList<>();
    list.add( 10 );
    list.add( 10 );
    list.add( 10 );
    list.add( 10 );
    // Error sólo acepta listas de Number
    // double n = boundedGeneric.sumBounded( list );
  }

  @Test
  public void testSumUpperBounded()
  {
    BoundedGeneric boundedGeneric = new BoundedGeneric();

    List<Number> list = new ArrayList<>();
    list.add( 10.0 );
    list.add( 10 );
    list.add( 10L );
    list.add( new BigDecimal( "10" ) );
    double n = boundedGeneric.sumUpperBounded( list );
    System.out.println( "sum = " + n );
  }
  
  @Test
  public void testSumUpperBounded2()
  {
    BoundedGeneric boundedGeneric = new BoundedGeneric();

    List<Double> list = new ArrayList<>();
    list.add( 10.1 );
    list.add( 10.9 );
    list.add( 10.8 );
    double n = boundedGeneric.sumUpperBounded( list );
    System.out.println( "sum = " + n );
  }
  
  @Test
  public void test() {
    List<?> a = new ArrayList<>();
 
  }

  @Test
  public void testSumUpperBounded_otherList()
  {
    BoundedGeneric boundedGeneric = new BoundedGeneric();

    List<Integer> list = new ArrayList<>();
    list.add( 10 );
    list.add( 10 );
    list.add( 10 );
    list.add( 10 );
    double n = boundedGeneric.sumUpperBounded( list );
    System.out.println( "sum = " + n );
  }

  @Test
  public void testPrintData()
  {
    BoundedGeneric boundedGeneric = new BoundedGeneric();

    List<Number> list = new ArrayList<>();
    list.add( 10.0 );
    list.add( 10 );
    list.add( 10L );
    list.add( new BigDecimal( "10" ) );
    boundedGeneric.printDataUnbounded( list );
  }

  @Test
  public void testPrintData_2()
  {
    BoundedGeneric boundedGeneric = new BoundedGeneric();

    List<String> list = new ArrayList<>();
    list.add( "ABC" );
    list.add( "qaws" );
    list.add( "10" );
    list.add( "..." );
    boundedGeneric.printDataUnbounded( list );
  }

  @Test
  public void testAddIntegersLowerBounded()
  {
    BoundedGeneric boundedGeneric = new BoundedGeneric();

    List<Integer> data = new ArrayList<>();
    boundedGeneric.addIntegersLowerBounded( data, 10 );
    boundedGeneric.addIntegersLowerBounded( data, 20 );
    boundedGeneric.addIntegersLowerBounded( data, 30 );
    boundedGeneric.addIntegersLowerBounded( data, 40 );
    boundedGeneric.addIntegersLowerBounded( data, 50 );

    for( Integer i : data )
    {
      System.out.println( i );
    }
  }
}
