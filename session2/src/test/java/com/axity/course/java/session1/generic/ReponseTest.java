package com.axity.course.java.session1.generic;

import org.junit.Test;

import com.axity.course.java.session2.exercise.Body;
import com.axity.course.java.session2.exercise.ErrorHeader;
import com.axity.course.java.session2.exercise.Header;
import com.axity.course.java.session2.exercise.OtherBody;
import com.axity.course.java.session2.exercise.Response;

public class ReponseTest
{

  @Test
  public void test() {
    
    Response<Body, Header> response = new Response<>();
    
    Body body = new Body();
    body.setA( 1 );
    response.setBody( body );
    Header header = new Header();
    header.setCode( "0001" );
    header.setId( 1 );
    response.setHeader( header  );
    
    System.out.println( response );
  }
  
  @Test
  public void test2() {
    
    Response<OtherBody, ErrorHeader> response = new Response<>();
    
    OtherBody body = new OtherBody();
    body.setB( 1 );
    response.setBody( body );
    ErrorHeader header = new ErrorHeader();
    header.setErrorCode( "E0001" );
    header.setDescription( "Lorem ipsum dolor sin amet" );
    response.setHeader( header  );
    
    System.out.println( response );
  }
}
