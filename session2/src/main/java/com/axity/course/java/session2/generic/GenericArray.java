package com.axity.course.java.session2.generic;

public class GenericArray<T>
{
  private T[] array;

  public T[] getArray()
  {
    return array;
  }

  public void setArray( T[] array )
  {
    this.array = array;
  }

}
