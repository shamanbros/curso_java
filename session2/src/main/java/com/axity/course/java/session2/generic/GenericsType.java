package com.axity.course.java.session2.generic;

public class GenericsType<T>
{
  private T t;

  public GenericsType()
  {
  }

  public GenericsType( T t )
  {
    this.t = t;
  }

  public T get()
  {
    return t;
  }

  public void set( T t )
  {
    this.t = t;
  }
}
