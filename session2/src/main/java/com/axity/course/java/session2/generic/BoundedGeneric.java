package com.axity.course.java.session2.generic;

import java.util.List;

public class BoundedGeneric
{

  public double sumBounded( List<Number> list )
  {
    double sum = 0;
    for( Number n : list )
    {
      sum += n.doubleValue();
    }
    return sum;
  }

  public double sumUpperBounded( List<? extends Number> list )
  {
    double sum = 0;
    for( Number n : list )
    {
      sum += n.doubleValue();
    }
    return sum;
  }
  

  public void printDataUnbounded( List<?> list )
  {
    for( Object obj : list )
    {
      System.out.println( "[" + obj + "]" );
    }
  }

  public void addIntegersLowerBounded( List<? super Integer> list, Integer i )
  {
    list.add( i );
  }
}
