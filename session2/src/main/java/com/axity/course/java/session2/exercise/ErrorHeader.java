package com.axity.course.java.session2.exercise;

public class ErrorHeader
{
  private String errorCode;
  private String description;

  public ErrorHeader()
  {
  }

  public ErrorHeader( String errorCode, String description )
  {
    this.errorCode = errorCode;
    this.description = description;
  }

  /**
   * @return the errorCode
   */
  public String getErrorCode()
  {
    return errorCode;
  }

  /**
   * @param errorCode the errorCode to set
   */
  public void setErrorCode( String errorCode )
  {
    this.errorCode = errorCode;
  }

  /**
   * @return the description
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription( String description )
  {
    this.description = description;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "[ errorCode :" ).append( this.errorCode ).append( ", " );
    sb.append( "description :" ).append( this.description ).append( "]" );
    return sb.toString();
  }
}
