package com.axity.course.java.session2.generic;

public class DemoGenericClass<T>
{
  private T t;

  public void set( T t )
  {
    this.t = t;
  }

  public T get()
  {
    return t;
  }
}
