package com.axity.course.java.session2.exercise;

public class Response<B, H>
{
  private H header;
  private B body;

  /**
   * @return the header
   */
  public H getHeader()
  {
    return header;
  }

  /**
   * @param header the header to set
   */
  public void setHeader( H header )
  {
    this.header = header;
  }

  /**
   * @return the body
   */
  public B getBody()
  {
    return body;
  }

  /**
   * @param body the body to set
   */
  public void setBody( B body )
  {
    this.body = body;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "[ header: " ).append( header );
    sb.append( ", body: " ).append( body ).append( "]" );

    return sb.toString();
  }
}
