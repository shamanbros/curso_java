package com.axity.course.java.session2.generic;

public class DemoServiceImpl implements DemoService<String, Integer>
{

  @Override
  public Integer doSomeOperation( String t )
  {
    return null;
  }

  @Override
  public String doReverseOperation( Integer t )
  {
    return null;
  }

}
