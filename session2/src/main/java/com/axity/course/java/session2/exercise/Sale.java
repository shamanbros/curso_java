package com.axity.course.java.session2.exercise;

import java.math.BigDecimal;
import java.util.Date;

public class Sale
{
  private String country;
  private int store;
  private String zone;
  private Date date;
  private BigDecimal sale;

  /**
   * @return the country
   */
  public String getCountry()
  {
    return country;
  }

  /**
   * @param country the country to set
   */
  public void setCountry( String country )
  {
    this.country = country;
  }

  /**
   * @return the store
   */
  public int getStore()
  {
    return store;
  }

  /**
   * @param store the store to set
   */
  public void setStore( int store )
  {
    this.store = store;
  }

  /**
   * @return the zone
   */
  public String getZone()
  {
    return zone;
  }

  /**
   * @param zone the zone to set
   */
  public void setZone( String zone )
  {
    this.zone = zone;
  }

  /**
   * @return the date
   */
  public Date getDate()
  {
    return date;
  }

  /**
   * @param date the date to set
   */
  public void setDate( Date date )
  {
    this.date = date;
  }

  /**
   * @return the sale
   */
  public BigDecimal getSale()
  {
    return sale;
  }

  /**
   * @param sale the sale to set
   */
  public void setSale( BigDecimal sale )
  {
    this.sale = sale;
  }

}
