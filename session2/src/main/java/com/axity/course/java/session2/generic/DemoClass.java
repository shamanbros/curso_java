package com.axity.course.java.session2.generic;

public class DemoClass
{
  private Object t;

  public void set( Object t )
  {
    this.t = t;
  }

  public Object get()
  {
    return t;
  }
}
