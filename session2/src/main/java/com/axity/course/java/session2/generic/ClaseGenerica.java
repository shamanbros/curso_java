package com.axity.course.java.session2.generic;

import java.util.List;

public class ClaseGenerica
{

  public void imprimeListaString( List<String> list )
  {
    for( String s : list )
    {
      System.out.println( s );
    }
  }

  public void imprimeListaNumeric( List<Number> list )
  {
    double n = 0.0;
    for( Number number : list )
    {
      n += number.doubleValue();
    }
    System.out.println( "Suma: " + n );
  }

}
