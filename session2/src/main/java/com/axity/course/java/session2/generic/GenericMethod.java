package com.axity.course.java.session2.generic;

/**
 * Java Generic Method
 * @author gsegura
 *
 */
public final class GenericMethod
{
  private GenericMethod()
  {
    // Se ofusca el constructor
  }

  public static <T> boolean isEqual( GenericsType<T> g1, GenericsType<T> g2 )
  {
    return g1.get().equals( g2.get() );
  }

  public static <T> boolean isEqual( T t1, T t2 )
  {
    return t1.equals( t2 );
  }
}
