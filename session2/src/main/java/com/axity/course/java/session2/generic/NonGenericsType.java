package com.axity.course.java.session2.generic;

public class NonGenericsType
{
  private Object t;

  public Object get()
  {
    return t;
  }

  public void set( Object t )
  {
    this.t = t;
  }
}
