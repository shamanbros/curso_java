package com.axity.course.java.session2.exercise;

import java.util.List;

public interface SaleService
{
  /**
   * Método que obtiene el listado de ventas
   * 
   * @return
   */
  List<Sale> getSales();
}
