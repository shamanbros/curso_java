package com.axity.course.java.session2.generic;

public interface DemoService<T1, T2>
{
  T2 doSomeOperation( T1 t );

  T1 doReverseOperation( T2 t );
}
