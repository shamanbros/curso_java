package com.axity.course.java.session2.generic;

import java.util.List;

public class ClaseNoGenerica
{

  public void imprimeLista( List list )
  {
    double n = 0.0;
    boolean isNumeric = false;
    for( Object obj : list )
    {
      if( obj instanceof String )
      {
        System.out.println( (String) obj );
      }
      if( obj instanceof Number )
      {
        isNumeric = true;
        n += ((Number) obj).doubleValue();
      }
    }
    if( isNumeric )
    {
      System.out.println( "Suma: " + n );
    }
  }
}
