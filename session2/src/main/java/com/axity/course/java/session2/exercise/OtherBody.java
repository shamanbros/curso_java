package com.axity.course.java.session2.exercise;

public class OtherBody
{

  private int b;

  /**
   * @return the b
   */
  public int getB()
  {
    return b;
  }

  /**
   * @param b the b to set
   */
  public void setB( int b )
  {
    this.b = b;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append( ", b : " ).append( b ).append( "]" );
    return sb.toString();
  }

}
