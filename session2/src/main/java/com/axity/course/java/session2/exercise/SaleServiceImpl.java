package com.axity.course.java.session2.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SaleServiceImpl implements SaleService
{

  @Override
  public List<Sale> getSales()
  {
    List<Sale> sales = new ArrayList<>();
    InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream( "sales.txt" );
    if( is != null )
    {

      BufferedReader rd = new BufferedReader( new InputStreamReader( is ) );

      try
      {
        String line = null;
        while( (line = rd.readLine()) != null )
        {
          Sale sale = transform( line );
          sales.add( sale );
        }
      }
      catch( IOException e )
      {
        e.printStackTrace();
      }
    }
    return sales;
  }

  private Sale transform( String line )
  {
    Sale sale = new Sale();
    String[] data = line.split( "[,]" );
    sale.setCountry( data[0] );
    sale.setStore( Integer.parseInt( data[1] ) );
    sale.setZone( data[2] );
    sale.setDate( transformDate( data[3] ) );
    sale.setSale( new BigDecimal( data[4] ) );
    return sale;
  }

  private Date transformDate( String value )
  {
    Date date = null;
    DateFormat df = new SimpleDateFormat( "yyyy-mm-dd" );
    try
    {
      date = df.parse( value );
    }
    catch( ParseException e )
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return date;
  }

}
