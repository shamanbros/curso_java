package com.axity.course.java.session2.generic;

public final class GenericUtil
{
  public static <T> int countAllOccurrences( T[] array, T item ) {
    int count = 0;
    if( item == null ) {
      for( T t : array )
        if( t == null )
          count++;
    } else {
      for( T t : array )
        if( item.equals( t ) )
          count++;
    }
    return count;
  }
}
